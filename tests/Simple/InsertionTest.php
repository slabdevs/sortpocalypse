<?php

namespace Slacademic\Tests\Sortpocalypse\Simple;

class InsertionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \Slacademic\Sortpocalypse\Simple\Insertion::sortArray
     *
     * @dataProvider dataProviderForSort
     */
    public function testSort($numberList)
    {
        $sort = new \Slacademic\Sortpocalypse\Simple\Insertion();

        //Make a copy to perform control sorting
        $controlNumbers = unserialize(serialize($numberList));
        sort($controlNumbers);

        $numberList = $sort->sortArray($numberList);

        $this->assertEquals($controlNumbers, $numberList);
    }

    /**
     * Generate some random numbers for the bubble sort
     *
     * @return array
     */
    public function dataProviderForSort()
    {
        $items = [];

        for ($k=0; $k<10; ++$k)
        {
            $items[$k] = [];
            $items[$k][0] = [];
            for ($i=0; $i<100; ++$i)
            {
                $items[$k][0][] = mt_rand(1, 200);
            }
        }

        return $items;
    }
}