<?php
/**
 * Test for Bubble Sort
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Tests\Sortpocalypse\Bubble;

class BubbleTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \Slacademic\Sortpocalypse\Bubble\Bubble::sortArray
     *
     * @dataProvider dataProviderForBubbleSort
     */
    public function testBubbleSort($numberList)
    {
        $bubbleSort = new \Slacademic\Sortpocalypse\Bubble\Bubble();

        //Make a copy to perform control sorting
        $controlNumbers = unserialize(serialize($numberList));
        sort($controlNumbers);

        $numberList = $bubbleSort->sortArray($numberList);

        $this->assertEquals($controlNumbers, $numberList);
    }

    /**
     * Generate some random numbers for the bubble sort
     *
     * @return array
     */
    public function dataProviderForBubbleSort()
    {
        $items = [];

        for ($k=0; $k<10; ++$k)
        {
            $items[$k] = [];
            $items[$k][0] = [];
            for ($i=0; $i<100; ++$i)
            {
                $items[$k][0][] = mt_rand(1, 200);
            }
        }

        return $items;
    }
}