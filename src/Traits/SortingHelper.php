<?php
/**
 * Sorting helper trait adds some simple helper functions
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Sortpocalypse\Traits;

trait SortingHelper
{
    /**
     * Simple swap function
     *
     * @param $array
     * @param $index1
     * @param $index2
     */
    private function swap(&$array, $index1, $index2)
    {
        $temp = $array[$index1];
        $array[$index1] = $array[$index2];
        $array[$index2] = $temp;
    }
}