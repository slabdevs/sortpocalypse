<?php
/**
 * Perform a bubble sort of an array. This loops through an array and does a swap on adjacent items until the loop
 * goes through without making a swap.
 *
 * Bubble sort is O(n^2) on average
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Sortpocalypse\Bubble;

class Bubble implements \Slacademic\Sortpocalypse\SortingInterface
{
    use \Slacademic\Sortpocalypse\Traits\SortingHelper;

    /**
     * Bubble sort array
     *
     * @param $array
     * @return mixed
     */
    public function sortArray($array)
    {
        $numberOfItems = count($array);

        $swapDone = true;
        while ($swapDone)
        {
            $swapDone = false;
            for ($i=0;$i<($numberOfItems - 1);++$i)
            {
                if ($array[$i] > $array[$i+1])
                {
                    $this->swap($array, $i, $i+1);
                    $swapDone = true;
                }
            }
        }

        return $array;
    }
}