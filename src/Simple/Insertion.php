<?php
/**
 * Insertion Sort, builds the sorted list as it goes. Average complexity is O(n^2)
 *
 * Outer loop goes over everything once. Inner loop pushes the current item back into it's correct place.
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Sortpocalypse\Simple;

class Insertion implements \Slacademic\Sortpocalypse\SortingInterface
{
    use \Slacademic\Sortpocalypse\Traits\SortingHelper;

    /**
     * Perform an "insertion" sort of an array
     *
     * @param $array
     * @return mixed
     */
    public function sortArray($array)
    {
        $numberOfItems = count($array);

        for ($i=1; $i<$numberOfItems; ++$i)
        {
            $j = $i;
            while ($j > 0 && $array[$j-1] > $array[$j])
            {
                $this->swap($array, $j, $j-1);
                $j--;
            }
        }

        return $array;
    }
}