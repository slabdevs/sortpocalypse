<?php
/**
 * Selection sort basically loops through the entire once and then at each index loops through the whole
 * rest of the items finding the smallest and then swapping it with the starting index of that subloop.
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Sortpocalypse\Simple;

class Selection implements \Slacademic\Sortpocalypse\SortingInterface
{
    use \Slacademic\Sortpocalypse\Traits\SortingHelper;

    /**
     * Sort array
     *
     * @param $array
     * @return mixed
     */
    public function sortArray($array)
    {
        $numberOfItems = count($array);

        for ($i=0; $i<$numberOfItems; ++$i)
        {
            $lowestIndex = -1;
            for ($j=$i; $j<$numberOfItems; ++$j)
            {
                if ($lowestIndex == -1 || $array[$j] < $array[$lowestIndex])
                {
                    $lowestIndex = $j;
                }
            }

            if ($lowestIndex != -1)
            {
                $this->swap($array, $i, $lowestIndex);
            }
        }

        return $array;
    }
}