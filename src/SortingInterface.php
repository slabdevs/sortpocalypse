<?php
/**
 * General Sorting Interface
 *
 * @package Slacademic
 * @subpackage Sortpocalypse
 * @author Eric
 */
namespace Slacademic\Sortpocalypse;

interface SortingInterface
{
    /**
     * Sort the array using whatever methodology the class decides to
     *
     * @param $array
     * @return mixed
     */
    public function sortArray($array);
}