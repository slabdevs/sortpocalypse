# Sortpocalypse

Writing some sorting algorithms along with unit tests to keep my aging developer brain in check.

## Testing

> phpunit --bootstrap vendor/autoload.php tests